package company.com.course_work.view.ui;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import company.com.course_work.model.Database.AppDatabase;
import company.com.course_work.model.Database.utils.Converter;
import company.com.course_work.R;
import company.com.course_work.model.entity.Currency;
import company.com.course_work.model.entity.CurrencyValue;
import company.com.course_work.viewmodel.adapter.CurrencyAdapter;
import company.com.course_work.viewmodel.BitmapTranslator;
import company.com.course_work.viewmodel.TimeManager;

public class Screen_5 extends AppCompatActivity {
    private BarChart chart;
    private AppDatabase appDatabase;
    private List<CurrencyValue> currencyValues;
    private Spinner spinner;
    private Currency currency;
    private CardView cardView;
    private CardView cardView2;
    private EditText startDateText;
    private EditText endDateText;
    private Date startDate;
    private Date endDate;
    private RecyclerView recyclerView;
    private View includeLayout;
    private CurrencyAdapter adapter;
    private List<Currency> currencyList;
    private Context context;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    Intent intent = new Intent(context, Screen_3.class);
                    startActivity(intent);
                    return true;
                case R.id.navigation_dashboard:
                    Intent intent1 = new Intent(context, Screen_1.class);
                    startActivity(intent1);
                    return true;
                case R.id.navigation_notifications:
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen_5);
        init();
        initRecyclerView();
        setListeners();
    }

    public void init() {
        context = this;

        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigation.getMenu().getItem(2).setChecked(true);

        currencyList = (ArrayList<Currency>) getIntent().getSerializableExtra("Currency");

        chart = findViewById(R.id.chart);
        chart.setVisibility(View.GONE);

        appDatabase = AppDatabase.getAppDatabase(getApplicationContext());

        spinner = findViewById(R.id.spinner_graph);
        spinner.setVisibility(View.GONE);

        cardView = findViewById(R.id.cardView);
        endDateText = cardView.findViewById(R.id.endDate);

        cardView.setVisibility(View.GONE);
        cardView2 = findViewById(R.id.cardView2);

        startDateText = cardView2.findViewById(R.id.startDate);
        cardView2.setVisibility(View.GONE);

        recyclerView = findViewById(R.id.main_recycler_view);

        includeLayout = findViewById(R.id.include3);
        includeLayout.setVisibility(View.GONE);
    }

    private void fetchData(int id) {
        if (appDatabase == null || currency == null)
            return;
        switch (id) {
            case 0:
                currencyValues = appDatabase.getCurValueDao().getAllById(currency.getId());
                break;
            case 1:
            case 2:
                currencyValues = appDatabase.getCurValueDao().findBetweenDatesByID(currency.getId(), startDate, endDate);
                break;
            case 3:
                currencyValues = appDatabase.getCurValueDao().getAllById(currency.getId());
            default:
                break;
        }
    }

    private void setListeners() {
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (id == 0) {
                    timePickerVisibility(false);
                }
                if (id == 1) {
                    timePickerVisibility(false);
                    endDate = new Date();
                    startDate = TimeManager.filterByWeek(endDate);
                }
                if (id == 2) {
                    timePickerVisibility(false);
                    endDate = new Date();
                    startDate = TimeManager.filterByMonth(endDate);
                }
                if (id == 3) {
                    timePickerVisibility(true);
                }
                if (currency != null) {
                    fetchData((int) id);
                    buildChart(currencyValues);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        startDateText.setOnClickListener(v -> showDatePickerDialog(v, startDateText));
        startDateText.setOnFocusChangeListener((v, hasFocus) -> {
            if(!hasFocus &&  startDateText.getText().toString() != "") {
                startDate = TimeManager.dateFromString(startDateText.getText().toString(), "/", true);
                DateTime start = new DateTime(startDate);
                DateTime end =  new DateTime(endDate);
                currencyValues.removeIf(currencyValue -> {
                    DateTime current = new DateTime(currencyValue.getDate());
                    return (!current.isAfter(start) && !current.isEqual(start))
                            || (!current.isBefore(end) && !current.isEqual(end));
                });
                buildChart(currencyValues);
            }
        });

        endDateText.setOnClickListener(v -> showDatePickerDialog(v, endDateText));
        endDateText.setOnFocusChangeListener((v, hasFocus) -> {
            if(!hasFocus &&  endDateText.getText().toString() != "") {
                startDate = TimeManager.dateFromString(startDateText.getText().toString(), "/", true);
                DateTime start = new DateTime(startDate);
                DateTime end =  new DateTime(endDate);
                currencyValues.removeIf(currencyValue -> {
                    DateTime current = new DateTime(currencyValue.getDate());
                    return (!current.isAfter(start) && !current.isEqual(start))
                            || (!current.isBefore(end) && !current.isEqual(end));
                });
                buildChart(currencyValues);
            }
        });
    }

    private void chartVisibility(boolean f) {
        if (f) {
            chart.setVisibility(View.VISIBLE);
            spinner.setVisibility(View.VISIBLE);
        }
        else {
            chart.setVisibility(View.GONE);
            spinner.setVisibility(View.GONE);
        }
    }

    private void timePickerVisibility(boolean f) {
        if (cardView == null || cardView2 == null)
             return;

        if (f) {
            cardView.setVisibility(View.VISIBLE);
            cardView2.setVisibility(View.VISIBLE);
        }
        else {
            cardView.setVisibility(View.GONE);
            cardView2.setVisibility(View.GONE);
        }
    }

    private void buildChart(List<CurrencyValue> currencies) {
        if (currencies.size() == 0)
            return;

        List<BarEntry> entries = new ArrayList<>();

        for (CurrencyValue data : currencies) {
            entries.add(new BarEntry(Converter.dateToTimestamp(data.getDate()), (float)data.getRate()));
        }

        BarDataSet dataSet = new BarDataSet(entries, "Label");
        dataSet.setColor(ContextCompat.getColor(this, R.color.colorAccent));
        dataSet.setValueTextColor(ContextCompat.getColor(this, R.color.colorPrimary));

        BarData lineData = new BarData(dataSet);
        chart.setData(lineData);
        chart.invalidate();
    }

    private void initRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        adapter = new CurrencyAdapter(null, currency -> {
            if (currency != null) {
                adapter.add(currency);
                adapter.sortCurrency();
            }

            this.currency = currency;

            Log.v("long", "click");

            TextView textView = includeLayout.findViewById(R.id.currency_name);
            textView.setText(currency.getName());
            ImageView imageView = includeLayout.findViewById(R.id.currency_logo);

            if (currency.getImage() != null)
                imageView.setImageDrawable(BitmapTranslator.bitmapToDrawable(BitmapTranslator.getImage(currency.getImage()), this));
            else
                imageView.setImageDrawable(getDrawable(R.drawable.ic_launcher_background));

            includeLayout.setBackgroundColor(ContextCompat.getColor(this, R.color.colorAccent));
            includeLayout.setVisibility(View.VISIBLE);
            adapter.remove(currency);

            fetchData(0);
            chartVisibility(true);

            buildChart(currencyValues);
            return true;
        }, appDatabase);
        recyclerView.setAdapter(adapter);
        adapter.setData(currencyList);
    }

    public void showDatePickerDialog(View v, EditText editText) {
        DatePickerFragment newFragment = new DatePickerFragment();
        newFragment.setEditText(editText);
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }
}
