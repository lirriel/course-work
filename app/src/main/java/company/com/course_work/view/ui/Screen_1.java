package company.com.course_work.view.ui;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import company.com.course_work.model.Database.AppDatabase;
import company.com.course_work.R;
import company.com.course_work.model.api.AppDelegate;
import company.com.course_work.model.entity.Currencies;
import company.com.course_work.model.entity.Currency;
import company.com.course_work.model.entity.CurrencyValue;
import company.com.course_work.model.entity.RatesList;
import company.com.course_work.viewmodel.adapter.CurrencyAdapter;
import company.com.course_work.viewmodel.BitmapTranslator;
import company.com.course_work.viewmodel.CurrencyListViewModel;
import company.com.course_work.viewmodel.TimeManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class Screen_1 extends AppCompatActivity {

    private CurrencyListViewModel viewModel;
    private Context context;
    private CurrencyAdapter adapter;
    private View errorView;
    private View contentView;
    private View loadingView;
    private View includeLayout;
    private AppDatabase database;
    private AppDelegate appDelegate;
    private Currency from;
    private int count = 0;
    private RecyclerView recyclerView;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    Intent intent = new Intent(context, Screen_3.class);
                    startActivity(intent);
                    return true;
                case R.id.navigation_dashboard:
                    return true;
                case R.id.navigation_notifications:
                    Intent intent1 = new Intent(context, Screen_5.class);
                    ArrayList<Currency> arrayList = new ArrayList<>(adapter.getData());
                    intent1.putParcelableArrayListExtra("Currency", arrayList);
                    startActivity(intent1);
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen_1);

        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigation.getMenu().getItem(1).setChecked(true);

        database = AppDatabase.getAppDatabase(getApplicationContext());
        context = this;
        initialize();

        fetchData();
        updateRate();
    }

    private void updateRate() {
        Date currentDate = new Date();
        database.getCurValueDao().deleteAll();
        Date lastRecord = database.getCurValueDao().getLastDate();
        if (!TimeManager.isSameDay(currentDate, lastRecord)) {
            StringBuilder symbols = new StringBuilder();
            adapter.getData().forEach(currency -> {
                symbols.append(currency.getSymbol());
                symbols.append(",");
            });
            symbols.deleteCharAt(symbols.length() - 1);
            String s = symbols.toString();
            appDelegate.getApiService()
                    .getRates(getResources().getString(R.string.api_key), s)
                    .enqueue(new Callback<RatesList>() {
                        @Override
                        public void onResponse(Call<RatesList> call, Response<RatesList> response) {
                            Log.v("loadRates", "success");
                            RatesList ratesList = response.body();
                            for (int i = 0; i < ratesList.getRates().size(); i++) {
                                String s = ratesList.getSymbols().get(i);
                                long id = database.getCurrencyDao().getCurrencySymbol(s).getId();

                                database.getCurValueDao().insert(
                                        new CurrencyValue(id, ratesList.getDate(), ratesList.getRates().get(i))
                                );
                            }
                        }

                        @Override
                        public void onFailure(Call<RatesList> call, Throwable t) {
                            Log.v("loadRates", "failed");
                            Timber.d(t);
                        }
                    });
        }
    }

    private void setViewModel() {
        CurrencyListViewModel.Factory factory = new CurrencyListViewModel.Factory(
                getApplication(), AppDelegate.from(this));
        viewModel = ViewModelProviders.of(this, factory).get(CurrencyListViewModel.class);
    }

    private void initialize() {
        //Toolbar toolbar = findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
        count = 0;
        errorView = findViewById(R.id.error_layout);
        contentView = findViewById(R.id.main_recycler_view);
        loadingView = findViewById(R.id.loading_layout);
        includeLayout = findViewById(R.id.include);
        initRecyclerView();
        appDelegate = AppDelegate.from(getApplicationContext());
    }

    private void loadCurrency() {
        AppDelegate.from(this)
                .getApiService()
                .getCurrency(getResources().getString(R.string.api_key))
                .enqueue(new Callback<Currencies>() {
                    @Override
                    public void onResponse(@NonNull Call<Currencies> call, @NonNull Response<Currencies> response) {
                        //  listMutableLiveData.setValue(response.body().getCurrenciesList());
                        database.getCurrencyDao().insertAll(response.body().getCurrenciesList());
                        adapter.setData(database.getCurrencyDao().getAllCurrency());
                        adapter.sortCurrency();
                        Log.v("load", "achieved");
                        Log.v("load", database.getCurrencyDao().countCurrency()+"");
                    }

                    @Override
                    public void onFailure(Call<Currencies> call, Throwable t) {
                        Timber.d(t);
                    }
                });
    }

    private void fetchData() {
        List<Currency> currencies = database.getCurrencyDao().getAllCurrency();

        if (currencies.size() == 0) {
            Log.v("data", "empty");
            loadCurrency();
        }
        else {
            Log.v("data", "NOT empty");
            adapter.setData(currencies);
            adapter.sortCurrency();
        }
    }

    private void subscribeToModel() {
        final Observer<List<company.com.course_work.model.entity.Currency>> nameObserver = coinInfoList -> {
            if (coinInfoList != null) {
                showData();
                adapter.setData(coinInfoList);
            }
            else
                showError();
        };

        viewModel.getListMutableLiveData().observe(this, nameObserver);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_refresh) {
            update();
            getData();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void showLoadingView() {
        loadingView.setVisibility(View.VISIBLE);
    }

    private void update() {
        showLoadingView();
        viewModel.loadCurrency();
    }

    private void getData() {
        showLoadingView();
        viewModel.getListMutableLiveData();
    }

    private void hideLoadingView() {
        loadingView.setVisibility(View.GONE);
    }

    private void showData() {
        hideLoadingView();
        errorView.setVisibility(View.GONE);
        contentView.setVisibility(View.VISIBLE);
    }

    private void showError() {
        hideLoadingView();
        contentView.setVisibility(View.GONE);
        errorView.setVisibility(View.VISIBLE);
    }

    private void initRecyclerView() {
        recyclerView = findViewById(R.id.main_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        adapter = new CurrencyAdapter(currency -> {
            Log.v("quick", "click");
            if (count == 1) {
                Intent intent = new Intent(this, Screen_2.class);
                intent.putExtra("From", (Parcelable) from);
                intent.putExtra("To", (Parcelable) currency);
                intent.putExtra("RV", false);
                startActivity(intent);
            }
            if (count == 0) {
                Intent intent = new Intent(this, Screen_2.class);
                intent.putExtra("RV", true);
                intent.putExtra("From", (Parcelable) currency);
                ArrayList<Currency> arrayList = new ArrayList<>(adapter.getData());
                intent.putParcelableArrayListExtra("Currency", arrayList);
                startActivity(intent);
            }
        }, currency -> {
            Log.v("long", "click");
            TextView textView = includeLayout.findViewById(R.id.currency_name);
            textView.setText(currency.getName());
            ImageView imageView = includeLayout.findViewById(R.id.currency_logo);
            if (currency.getImage() != null)
                imageView.setImageDrawable(BitmapTranslator.bitmapToDrawable(BitmapTranslator.getImage(currency.getImage()), this));
            else
                imageView.setImageDrawable(getDrawable(R.drawable.ic_launcher_background));
            includeLayout.setBackgroundColor(ContextCompat.getColor(this, R.color.colorAccent));
            includeLayout.setVisibility(View.VISIBLE);
            adapter.remove(currency);
            count = 1;
            from = currency;
            return true;
        }, database);
        recyclerView.setAdapter(adapter);
    }
}
