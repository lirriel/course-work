package company.com.course_work.view.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import company.com.course_work.model.Database.AppDatabase;
import company.com.course_work.R;
import company.com.course_work.model.api.AppDelegate;
import company.com.course_work.model.entity.Change;
import company.com.course_work.model.entity.Currency;
import company.com.course_work.viewmodel.NetworkChangeReceiver;
import company.com.course_work.viewmodel.adapter.CurrencyAdapter;
import company.com.course_work.viewmodel.BitmapTranslator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class Screen_2 extends AppCompatActivity {

    private EditText textTo;
    private EditText textFrom;
    private Button button;
    private View fromView;
    private View toView;
    private Change change;
    private Currency from;
    private double rateFrom = 1;
    private double rateTo = 1;
    private Currency to;
    private AppDelegate appDelegate;
    private AppDatabase appDatabase;
    private RecyclerView recyclerView;
    private CurrencyAdapter adapter;
    private List<Currency> currencyList;
    private Context context;
    private NetworkChangeReceiver networkChangeReceiver;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = item -> {
                switch (item.getItemId()) {
                    case R.id.navigation_home:
                        Intent intent2 = new Intent(context, Screen_3.class);
                        startActivity(intent2);
                        return true;
                    case R.id.navigation_dashboard:
                        Intent intent = new Intent(context, Screen_1.class);
                        startActivity(intent);
                        return true;
                    case R.id.navigation_notifications:
                        Intent intent1 = new Intent(context, Screen_5.class);
                        startActivity(intent1);
                        return true;
                }
                return false;
            };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen_2);

        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigation.getMenu().getItem(1).setChecked(true);

        init();

        from = (Currency) getIntent().getSerializableExtra("From");
        setView(fromView, from);

        if (getIntent().getBooleanExtra("RV", false)) {
            recyclerView.setVisibility(View.VISIBLE);
            textTo.setVisibility(View.GONE);
            textFrom.setVisibility(View.GONE);
            toView.setVisibility(View.GONE);
            button.setVisibility(View.GONE);
            currencyList = (ArrayList<Currency>) getIntent().getSerializableExtra("Currency");
            initRecyclerView();
            adapter.setData(currencyList);
            adapter.sortCurrency();
        }
        else {
            to = (Currency) getIntent().getSerializableExtra("To");
            change = new Change(from.getId(), 1, to.getId(), 1);

            change.setFromCurrencyID(appDatabase.getCurrencyDao().getId(from.getSymbol()));
            change.setToCurrencyID(appDatabase.getCurrencyDao().getId(to.getSymbol()));
            loadRate();

            setView(toView, to);
        }
        setListeners();
    }

    void showExchange() {
        recyclerView.setVisibility(View.GONE);
        textTo.setVisibility(View.VISIBLE);
        textFrom.setVisibility(View.VISIBLE);
        toView.setVisibility(View.VISIBLE);
        button.setVisibility(View.VISIBLE);
    }

    private void initRecyclerView() {
        networkChangeReceiver = new NetworkChangeReceiver();
        networkChangeReceiver.setButton(button);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new CurrencyAdapter(currency -> {
            showExchange();
            to = currency;
            change = new Change(from.getId(), 1, to.getId(), 1);

            change.setFromCurrencyID(appDatabase.getCurrencyDao().getId(from.getSymbol()));
            change.setToCurrencyID(appDatabase.getCurrencyDao().getId(to.getSymbol()));
            loadRate();

            setView(toView, to);
        }, null, AppDatabase.getAppDatabase(getApplicationContext()));
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
    }

    public void loadRate() {
        button.setEnabled(false);
        appDelegate.getApiService()
                .getConvert(getResources().getString(R.string.api_key), from.getSymbol() + "," + to.getSymbol())
                .enqueue(new Callback<Change>() {
                    @Override
                    public void onResponse(Call<Change> call, Response<Change> response) {
                        Change change1 = response.body();

                        rateTo = change1.getToValue() / change1.getFromValue();
                        rateFrom = change1.getFromValue() / change1.getToValue();

                        textTo.setEnabled(true);
                        textFrom.setEnabled(true);

                        button.setEnabled(true);
                    }

                    @Override
                    public void onFailure(Call<Change> call, Throwable t) {
                        Timber.d(t);
                        button.setEnabled(false);
                    }
                });
    }

    public void setView(View view, Currency currency) {
        TextView textView = view.findViewById(R.id.currency_name);
        textView.setText(currency.getName());
        ImageView imageView = view.findViewById(R.id.currency_logo);
        if (currency.getImage() != null)
            imageView.setImageDrawable(BitmapTranslator.bitmapToDrawable(BitmapTranslator.getImage(currency.getImage()), this));
        else
            imageView.setImageDrawable(getDrawable(R.drawable.ic_launcher_background));
        view.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        view.setVisibility(View.VISIBLE);
    }

    public void init() {
        context = this;
        recyclerView = findViewById(R.id.main_recycler_view);
        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        appDatabase = AppDatabase.getAppDatabase(getApplicationContext());

        fromView = findViewById(R.id.includeTop);
        toView = findViewById(R.id.includeBottom);
        appDelegate = AppDelegate.from(this);
        textFrom = findViewById(R.id.editText);
        textTo = findViewById(R.id.editText2);

        textTo.setEnabled(true);
        textFrom.setEnabled(true);

        button = findViewById(R.id.button);
    }

    public void fetchChange(String editText, EditText setText, boolean switcher) {
        double value;
        try {
            value = Double.parseDouble(editText);
        }
        catch (NumberFormatException e) {
            return;
        }

        if (switcher)
            change.setToValue(rateTo * value);
        else
            change.setToValue(rateFrom * value);
        change.setFromValue(value);
        setText.setText(String.format("%.2f", change.getToValue()));
    }

    public void setListeners() {
        textTo.setOnClickListener(v -> {
            fetchChange(((EditText)v).getText().toString(), textFrom, true);
            loadRate();
        });

        textFrom.setOnClickListener(v -> {
            fetchChange(((EditText)v).getText().toString(), textTo, true);
            loadRate();
        });

        button.setOnClickListener(v -> {
            change.setDate(new Date());
            Change change1 = new Change(change);
            Log.v("save change, date: ", change1.getDate().toString());
            appDatabase.getChangeDao().insert(change1);
            loadRate();
        });
    }

}
