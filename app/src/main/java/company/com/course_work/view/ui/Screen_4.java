package company.com.course_work.view.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

import company.com.course_work.model.Database.AppDatabase;
import company.com.course_work.R;
import company.com.course_work.model.entity.Currency;
import company.com.course_work.viewmodel.adapter.CurrencyCheckAdapter;
import company.com.course_work.viewmodel.TimeManager;

public class Screen_4 extends AppCompatActivity {

    private EditText startDate;
    private EditText endDate;
    private long[] currencies;
    private CurrencyCheckAdapter adapter;
    private RecyclerView recyclerView;
    private AppDatabase appDatabase;
    private Spinner spinner;
    private Button button;
    private long spinnerId;
    private List<Currency> currencyList;
    private CardView cardView1, cardView2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen_4);

        init();
        setListeners();
        initRecyclerView();
        fetchData();
    }

    public void init() {
        startDate = findViewById(R.id.startDate);
        endDate = findViewById(R.id.endDate);
        button = findViewById(R.id.button2);
        currencies = getIntent().getLongArrayExtra("Currency");
        spinner = findViewById(R.id.spinner);
        recyclerView = findViewById(R.id.check_recycler_view);
        appDatabase = AppDatabase.getAppDatabase(getApplicationContext());

        cardView1 = findViewById(R.id.cardView);
        cardView2 = findViewById(R.id.cardView2);

        timePickerVisibility(false);
    }

    private void timePickerVisibility(boolean f) {
        if (f) {
            cardView1.setVisibility(View.VISIBLE);
            cardView2.setVisibility(View.VISIBLE);
        }
        else {
            cardView1.setVisibility(View.GONE);
            cardView2.setVisibility(View.GONE);
        }
    }

    public void setListeners() {
        startDate.setOnClickListener(v -> showDatePickerDialog(v, startDate));

        endDate.setOnClickListener(v -> showDatePickerDialog(v, endDate));

        button.setOnClickListener(v->{
            Intent intent = new Intent();
            intent.putExtra("time", spinnerId);
            if (spinnerId == 3) {
                intent.putExtra("startTime", TimeManager.dateFromString(startDate.getText().toString(), "/", true));
                intent.putExtra("endTime", TimeManager.dateFromString(endDate.getText().toString(), "/", true));
            }
            currencyList.removeIf(currency -> !currency.isCheck());
            ArrayList<Currency> arrayList = new ArrayList<>(currencyList);
            intent.putParcelableArrayListExtra("Currency", arrayList);
            setResult(RESULT_OK, intent);
            finish();
        });

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                spinnerId = id;
                if (id == 0) {
                    timePickerVisibility(false);
                    return;
                }
                if (id == 1) {
                    timePickerVisibility(false);
                    return;
                }
                if (id == 2) {
                    timePickerVisibility(false);
                    return;
                }
                if (id == 3) {
                    timePickerVisibility(true);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void showDatePickerDialog(View v, EditText editText) {
        DatePickerFragment newFragment = new DatePickerFragment();
        newFragment.setEditText(editText);
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    private void initRecyclerView() {
        recyclerView = findViewById(R.id.check_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        adapter = new CurrencyCheckAdapter(currency -> currency.setCheck(true));
        recyclerView.setAdapter(adapter);
    }

    private void fetchData() {
        currencyList = new ArrayList<>();
        for (long id: currencies) {
            currencyList.add(appDatabase.getCurrencyDao().getCurrencyByID(id));
        }
        adapter.setData(currencyList);
    }

    /*public static class DatePickerFragment extends DialogFragment implements
            DatePickerDialog.OnDateSetListener {

        private EditText editText;

        public void setEditText(EditText editText) {
            this.editText = editText;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            editText.setText(dayOfMonth + "/" + (month + 1) + "/" + year);
        }
    }*/

}
