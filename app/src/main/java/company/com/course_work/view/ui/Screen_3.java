package company.com.course_work.view.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Button;

import com.robertlevonyan.views.chip.Chip;
import com.wefika.flowlayout.FlowLayout;

import net.danlew.android.joda.JodaTimeAndroid;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import company.com.course_work.model.Database.AppDatabase;
import company.com.course_work.R;
import company.com.course_work.model.entity.Change;
import company.com.course_work.model.entity.Currency;
import company.com.course_work.viewmodel.adapter.ChangeAdapter;

import static company.com.course_work.viewmodel.TimeManager.filterByMonth;
import static company.com.course_work.viewmodel.TimeManager.filterByWeek;

public class Screen_3 extends AppCompatActivity {

    private AppDatabase appDatabase;
    private ChangeAdapter adapter;
    private Button button;
    private List<Change> changes;
    private List<Change> changeInitialList;
    private HashSet<Long> currencies;
    private Date startDate;
    private Date endDate;
    private FlowLayout settingLayout;
    private Context context;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = item -> {
                switch (item.getItemId()) {
                    case R.id.navigation_home:
                        return true;
                    case R.id.navigation_dashboard:
                        Intent intent = new Intent(context, Screen_1.class);
                        startActivity(intent);
                        return true;
                    case R.id.navigation_notifications:
                        Intent intent1 = new Intent(context, Screen_5.class);
                        startActivity(intent1);
                        return true;
                }
                return false;
            };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen_3);

        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigation.getMenu().getItem(0).setChecked(true);
        context = this;

        appDatabase = AppDatabase.getAppDatabase(getApplicationContext());
        JodaTimeAndroid.init(this);
        initRecyclerView();
        fetchData();
        setListeners();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            long id = data.getLongExtra("time", 0);
            Chip chip = new Chip(this);
            chip.setClosable(true);
            chip.changeBackgroundColor(ContextCompat.getColor(this, R.color.colorAccent));
            if (id == 0) {
                chip.setChipText("All time");
            }
            if (id == 1) {
                endDate = new Date();
                startDate = filterByWeek(endDate);
                chip.setChipText("Week");
            }
            if (id == 2) {
                endDate = new Date();
                startDate = filterByMonth(endDate);
                chip.setChipText("Month");
            }
            if (id == 3) {
                startDate = (Date)data.getSerializableExtra("startTime");
                endDate = (Date)data.getSerializableExtra("endTime");

                chip.setChipText("Custom time");
                Chip chipStart = new Chip(this);
                chipStart.setClosable(true);
                chipStart.setChipText(startDate.toString());
                settingLayout.addView(chipStart);
                chipStart.changeBackgroundColor(ContextCompat.getColor(this, R.color.colorAccent));

                chipStart = new Chip(this);
                chipStart.setClosable(true);
                chipStart.setChipText(endDate.toString());
                settingLayout.addView(chipStart);
                chipStart.changeBackgroundColor(ContextCompat.getColor(this, R.color.colorAccent));
            }
            settingLayout.addView(chip);
            List<Currency> currencyList = data.getParcelableArrayListExtra("Currency");
            currencyList.forEach(currency -> {
                Chip chip1 = new Chip(this);
                chip1.setChipText(currency.getSymbol());
                chip1.setClosable(true);
                settingLayout.addView(chip1);
                chip1.changeBackgroundColor(ContextCompat.getColor(this, R.color.colorAccent));
            });
            changes = new ArrayList<>(changeInitialList);
            if (currencyList.size() > 0)
                changes.removeIf(change -> {
                    for (Currency c: currencyList) {
                        if (c.getId() == change.getFromCurrencyID() || c.getId() == change.getToCurrencyID())
                            return false;
                    }
                    return true;
                });
            if (id != 0)
            {
                DateTime start = new DateTime(startDate);
                DateTime end =  new DateTime(endDate);

                changes.removeIf(change -> {
                    DateTime current = new DateTime(change.getDate());
                    return (!current.isAfter(start) && !current.isEqual(start))
                            || (!current.isBefore(end) && !current.isEqual(end));
                });
            }
            adapter.setData(changes);
        }
        else {
            Log.v("Error", "result code failed");
        }
    }

    private void initRecyclerView() {
        settingLayout = findViewById(R.id.constraintLayout2);
        RecyclerView recyclerView = findViewById(R.id.history_recycler_view);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new ChangeAdapter(appDatabase);
        recyclerView.setAdapter(adapter);
        button = findViewById(R.id.buttonFilter);
        currencies = new HashSet<>();
    }

    private void fetchData() {
        changeInitialList = appDatabase.getChangeDao().getAllChange();
        adapter.setData(changeInitialList);

        for (Change change: changeInitialList) {
            currencies.add(change.getFromCurrencyID());
            currencies.add(change.getToCurrencyID());
        }
    }

    public void setListeners() {
        button.setOnClickListener(v-> {
            Intent intent = new Intent(v.getContext(), Screen_4.class);
            long[] arr = new long[currencies.size()];
            AtomicInteger i = new AtomicInteger();

            currencies.forEach(aLong -> {
                arr[i.get()] = aLong;
                i.set(i.get() + 1);
            });

            intent.putExtra("Currency", arr);
            startActivityForResult(intent, 1);
        });
    }
}
