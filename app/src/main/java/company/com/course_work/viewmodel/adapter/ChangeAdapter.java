package company.com.course_work.viewmodel.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import company.com.course_work.model.Database.AppDatabase;
import company.com.course_work.R;
import company.com.course_work.model.entity.Change;
import company.com.course_work.model.entity.Currency;

public class ChangeAdapter extends RecyclerView.Adapter<ChangeAdapter.ChangeViewHolder> {

    private List<Change> items = new ArrayList<>();
    private static AppDatabase appDatabase;

    public ChangeAdapter(AppDatabase appDatabase) {
        ChangeAdapter.appDatabase = appDatabase;
    }

    @NonNull
    @Override
    public ChangeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layout = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_history, parent, false);
        return new ChangeAdapter.ChangeViewHolder(layout);
    }

    @Override
    public void onBindViewHolder(@NonNull ChangeViewHolder holder, int position) {
        holder.bind(items.get(position));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setData(List<Change> newItems) {
        items = newItems;
        notifyDataSetChanged();
    }

    static class ChangeViewHolder extends RecyclerView.ViewHolder {

        Context context;
        TextView currencyFrom;
        TextView valueFrom;
        TextView currencyTo;
        TextView valueTo;
        TextView date;

        ChangeViewHolder(View itemView) {
            super(itemView);
            context = itemView.getContext();
            currencyFrom = itemView.findViewById(R.id.textViewFromCurrency);
            currencyTo = itemView.findViewById(R.id.textViewToCurrency);
            valueFrom = itemView.findViewById(R.id.textViewFromValue);
            valueTo = itemView.findViewById(R.id.textViewToValue);
            date = itemView.findViewById(R.id.textViewDate);
        }

        void bind(Change change) {
            Currency currency = appDatabase.getCurrencyDao().getCurrencyByID(change.getFromCurrencyID());
            currencyFrom.setText(currency.getSymbol());
            currency = appDatabase.getCurrencyDao().getCurrencyByID(change.getToCurrencyID());
            currencyTo.setText(currency.getSymbol());
            valueFrom.setText("" + change.getFromValue());
            valueTo.setText("" + change.getToValue());
            date.setText(change.getDate().toString());
        }
    }
}
