package company.com.course_work.viewmodel.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;
import java.util.List;

import company.com.course_work.model.Database.AppDatabase;
import company.com.course_work.R;
import company.com.course_work.model.entity.Currency;
import company.com.course_work.viewmodel.BitmapTranslator;

public class CurrencyAdapter extends RecyclerView.Adapter<CurrencyAdapter.CurrencyViewHolder> {

    private List<Currency> items = new ArrayList<>();

    private OnItemClickListener listener;
    private OnItemLongClickListener longClickListener;
    private static AppDatabase appDatabase;

    public CurrencyAdapter(OnItemClickListener listener, OnItemLongClickListener longClickListener, AppDatabase appDatabase) {
        this.listener = listener;
        this.longClickListener = longClickListener;
        CurrencyAdapter.appDatabase = appDatabase;
    }

    @NonNull
    @Override
    public CurrencyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layout = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_currency, parent, false);
        return new CurrencyViewHolder(layout, listener, longClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull CurrencyViewHolder holder, int position) {
        holder.bind(items.get(position));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setData(List<Currency> newItems) {
        items = newItems;
        notifyDataSetChanged();
    }

    public List<Currency> getData() {
        return items;
    }

    public void remove(Currency item) {
        int position = items.indexOf(item);
        items.remove(position);
        notifyItemRemoved(position);
    }

    public void add(Currency item) {
        items.add(item);
        notifyItemInserted(items.size() - 1);
    }

    public void sortCurrency() {
        items.sort((o1, o2) -> {
            return -Boolean.compare(o1.isFavourite(), o2.isFavourite());
           /* if (o1.isFavourite() && o2.isFavourite())
                return 0;
            if (o1.isFavourite() && !o2.isFavourite())
                return -1;
            return 1;*/
        });
        notifyDataSetChanged();
    }

    static class CurrencyViewHolder extends RecyclerView.ViewHolder {

        Context context;
        OnItemClickListener listener;
        OnItemLongClickListener longClickListener;
        TextView currencyName;
        ImageView coinLogo;
        Button favourite;

        CurrencyViewHolder(View itemView, OnItemClickListener listener, OnItemLongClickListener longClickListener) {
            super(itemView);

            Log.v("viewHolder", "created");
            context = itemView.getContext();

            this.listener = listener;
            this.longClickListener = longClickListener;

            currencyName = itemView.findViewById(R.id.currency_name);
            coinLogo = itemView.findViewById(R.id.currency_logo);
            favourite = itemView.findViewById(R.id.button_favourite);
        }

        void setFave(boolean isFavourite) {
            if (isFavourite)
                favourite.setBackground(context.getResources().getDrawable(android.R.drawable.star_big_on));
            else
                favourite.setBackground(context.getResources().getDrawable(android.R.drawable.star_big_off));
        }

        void bind(Currency currencyInfo) {
            Log.v("bind", "started");
            if (listener != null)
                itemView.setOnClickListener(view -> listener.onClick(currencyInfo));
            if (longClickListener != null)
                itemView.setOnLongClickListener(view -> longClickListener.onLongClick(currencyInfo));

            currencyName.setText(currencyInfo.getSymbol());

            favourite.setOnClickListener(v -> {
                if (currencyInfo.isFavourite())
                    currencyInfo.setFavourite(false);
                else
                    currencyInfo.setFavourite(true);
                setFave(currencyInfo.isFavourite());
                appDatabase.getCurrencyDao().updateCurrencyFave(currencyInfo.getSymbol(), currencyInfo.isFavourite());
                Log.v("fave", "added");
            });

            setFave(currencyInfo.isFavourite());

            String logoUrl = context.getString(R.string.coin_logo_url, currencyInfo.getSymbol().toLowerCase());

            RequestOptions options = new RequestOptions()
                    .centerCrop()
                    .placeholder(R.mipmap.ic_launcher_round)
                    .fallback(android.R.color.holo_orange_light)
                    .error(R.mipmap.ic_launcher_round);

            byte[] image = appDatabase.getCurrencyDao().getImage(currencyInfo.getSymbol());
            if (image != null)
            {
                coinLogo.setImageDrawable(BitmapTranslator.bitmapToDrawable(BitmapTranslator.getImage(image), context));
                Log.v("bind", "load from db");
            }
            else {
                Glide.with(itemView)
                        .load(logoUrl)
                        .apply(options)
                        .listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                currencyInfo.setImage(BitmapTranslator.getBitmapAsByteArray(BitmapTranslator.drawableToBitmap(resource)));
                                Log.v("bind", "load success");
                                appDatabase.getCurrencyDao().updateCurrencyImage(currencyInfo.getSymbol(), currencyInfo.getImage());
                                return false;
                            }
                        })
                        .into(coinLogo);
            }
        }
    }

    public interface OnItemClickListener {
        void onClick(Currency currency);
    }

    public interface OnItemLongClickListener {
        boolean onLongClick(Currency currency);
    }
}
