package company.com.course_work.viewmodel;

import android.annotation.SuppressLint;
import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;


import java.util.List;

import company.com.course_work.model.api.AppDelegate;
import company.com.course_work.model.entity.Currencies;
import company.com.course_work.model.entity.Currency;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class CurrencyListViewModel extends AndroidViewModel {
    private MutableLiveData<List<Currency>> listMutableLiveData;
    @SuppressLint("StaticFieldLeak")
    private AppDelegate appDelegate;
    private String apiKey="2fffd42731d529099576948f4676d1e1";

    CurrencyListViewModel(@NonNull Application application, AppDelegate appDelegate) {
        super(application);
        this.appDelegate = appDelegate;
    }

    public LiveData<List<Currency>> getListMutableLiveData() {
        if (listMutableLiveData == null) {
            listMutableLiveData = new MutableLiveData<>();
            //loadCurrency();
        }
        return listMutableLiveData;
    }

    public void loadCurrency() {
        appDelegate
                .getApiService()
                .getCurrency(apiKey)
                .enqueue(new Callback<Currencies>() {
                    @Override
                    public void onResponse(@NonNull Call<Currencies> call, @NonNull Response<Currencies> response) {
                     //  listMutableLiveData.setValue(response.body().getCurrenciesList());
                    }

                    @Override
                    public void onFailure(Call<Currencies> call, Throwable t) {
                        Timber.d(t);
                    }
                });
    }

    public static class Factory extends ViewModelProvider.NewInstanceFactory {

        @NonNull
        private final Application application;

        private final AppDelegate appDelegate;

        public Factory(@NonNull Application application, AppDelegate appDelegate) {
            this.application = application;
            this.appDelegate = appDelegate;
        }

        @Override
        public <T extends ViewModel> T create(Class<T> modelClass) {
            return (T) new CurrencyListViewModel(application, appDelegate);
        }
    }
}