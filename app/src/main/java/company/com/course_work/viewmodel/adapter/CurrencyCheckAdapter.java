package company.com.course_work.viewmodel.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import company.com.course_work.R;
import company.com.course_work.model.entity.Currency;
import company.com.course_work.viewmodel.BitmapTranslator;

public class CurrencyCheckAdapter extends RecyclerView.Adapter<CurrencyCheckAdapter.CurrencyCheckViewHolder> {

    private List<Currency> items = new ArrayList<>();

    private OnItemClickListener listener;

    public CurrencyCheckAdapter(OnItemClickListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public CurrencyCheckViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layout = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.check_item_, parent, false);
        return new CurrencyCheckViewHolder(layout, listener);
    }

    @Override
    public void onBindViewHolder(@NonNull CurrencyCheckViewHolder holder, int position) {
        holder.bind(items.get(position));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setData(List<Currency> newItems) {
        items = newItems;
        notifyDataSetChanged();
    }

    static class CurrencyCheckViewHolder extends RecyclerView.ViewHolder {

        Context context;
        OnItemClickListener listener;
        TextView currencyName;
        ImageView coinLogo;
        CheckBox checkBox;

        CurrencyCheckViewHolder(View itemView, OnItemClickListener listener) {
            super(itemView);
            context = itemView.getContext();
            this.listener = listener;
            currencyName = itemView.findViewById(R.id.currency_name_check);
            coinLogo = itemView.findViewById(R.id.currency_logo_check);
            checkBox = itemView.findViewById(R.id.checkBox_check);
        }

        void bind(Currency currencyInfo) {
            currencyName.setText(currencyInfo.getName());
            if (currencyInfo.getImage() != null)
                coinLogo.setImageDrawable(BitmapTranslator.bitmapToDrawable(BitmapTranslator.getImage(currencyInfo.getImage()), context));
            else
                coinLogo.setImageDrawable(context.getDrawable(R.drawable.ic_launcher_foreground));
            checkBox.setOnClickListener(view -> listener.onClick(currencyInfo));
        }
    }

    public interface OnItemClickListener {
        void onClick(Currency currency);
    }
}
