package company.com.course_work.viewmodel;

import org.apache.commons.lang.time.DateUtils;
import org.joda.time.DateTime;

import java.util.Date;

public class TimeManager {
    public static Date filterByMonth(Date date) {
        DateTime dateTime = new DateTime(date);
        dateTime = dateTime.minusMonths(1);
        return dateTime.toDate();
    }

    public static Date filterByWeek(Date date) {
        DateTime dateTime = new DateTime(date);
        dateTime = dateTime.minusWeeks(1);
        return dateTime.toDate();
    }

    public static boolean isSameDay(Date date1, Date date2) {
        if (date1 == null || date2 == null)
            return false;

        return DateUtils.isSameDay(date1, date2);
    }

    public static Date dateFromString(String line, String separator, boolean switcher) {
        String arr[] = line.split(separator);

        if (switcher)
            return new Date(Integer.parseInt(arr[2]), Integer.parseInt(arr[1]) - 1, Integer.parseInt(arr[0]), 0, 0, 0);

        return new Date(Integer.parseInt(arr[0]), Integer.parseInt(arr[1]) - 1, Integer.parseInt(arr[2]), 0, 0, 0);
    }
}
