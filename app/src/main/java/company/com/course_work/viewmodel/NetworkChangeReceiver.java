package company.com.course_work.viewmodel;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Button;

import company.com.course_work.model.network.NetworkUtil;

public class NetworkChangeReceiver extends BroadcastReceiver {

    private Button button;

    public void setButton(Button button) {
        this.button = button;
    }

    @Override
    public void onReceive(final Context context, final Intent intent) {

        int status = NetworkUtil.getConnectivityStatusString(context);

        if (!"android.net.conn.CONNECTIVITY_CHANGE".equals(intent.getAction())) {
            if(status == NetworkUtil.NETWORK_STATUS_NOT_CONNECTED){
                Log.v("internet", "on");
                button.setEnabled(true);
            } else {
                Log.v("internet", "off");
                button.setEnabled(false);
            }
        }
    }
}
