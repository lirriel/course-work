package company.com.course_work.model.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RatesList implements Serializable{
    @SerializedName("date")
    @Expose
    private Date date;

    @SerializedName("symbols")
    @Expose
    private List<String> symbols;

    @SerializedName("rates")
    @Expose
    private List<Double> rates;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void addPair(String symbol, double rate) {
        symbols.add(symbol);
        rates.add(rate);
    }

    public RatesList() {
        symbols = new ArrayList<>();
        rates = new ArrayList<>();
    }
    public List<String> getSymbols() {
        return symbols;
    }

    public void setSymbols(List<String> symbols) {
        this.symbols = symbols;
    }

    public List<Double> getRates() {
        return rates;
    }

    public void setRates(List<Double> rates) {
        this.rates = rates;
    }
}
