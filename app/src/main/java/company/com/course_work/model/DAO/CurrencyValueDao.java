package company.com.course_work.model.DAO;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.Date;
import java.util.List;

import company.com.course_work.model.entity.CurrencyValue;

@Dao
public interface CurrencyValueDao {
    @Insert
    void insertAll(CurrencyValue... currencies);

    @Query("DELETE FROM currencyvalue")
    void deleteAll();

    @Delete
    void delete(CurrencyValue currencyValue);

    @Insert
    void insert(CurrencyValue currencyValue);

    // Получение всех Person из бд
    @Query("SELECT * FROM currencyvalue")
    List<CurrencyValue> getAllCurrencyValue();

    @Query("SELECT COUNT(*) from currencyvalue")
    int countCurrencyValues();

    @Query("SELECT * FROM currencyvalue WHERE date BETWEEN :from AND :to")
    List<CurrencyValue> findBetweenDates(Date from, Date to);

    @Query("SELECT * FROM currencyvalue WHERE currencyId=:curId")
    List<CurrencyValue> getAllById(long curId);

    @Query("SELECT MAX(date) FROM currencyvalue")
    Date getLastDate();

    @Query("SELECT * FROM currencyvalue WHERE (date BETWEEN :from AND :to) AND (currencyId=:curId)")
    List<CurrencyValue> findBetweenDatesByID(long curId, Date from, Date to);
}
