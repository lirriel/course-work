package company.com.course_work.model.api.deserializer;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import company.com.course_work.model.entity.Change;

public class ChangeDeserializer implements JsonDeserializer<Change> {

    @Override
    public Change deserialize(
            JsonElement json,
            Type typeOfT,
            JsonDeserializationContext context
    ) throws JsonParseException {

        if (json.isJsonObject()) {
            JsonObject jsonObject = json.getAsJsonObject();
            Set<Map.Entry<String, JsonElement>> entries = jsonObject.get("rates").getAsJsonObject().entrySet();

            List<String> names = new ArrayList<>();
            List<Double>  values = new ArrayList<>();
            for (Map.Entry<String, JsonElement> entry : entries) {
                names.add(entry.getKey());
                values.add(entry.getValue().getAsDouble());
            }

            return new Change(0, values.get(0), 0, values.get(1));
        }

        return null;
    }
}
