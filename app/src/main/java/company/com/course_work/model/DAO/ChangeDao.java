package company.com.course_work.model.DAO;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.Date;
import java.util.List;

import company.com.course_work.model.entity.Change;
import company.com.course_work.model.entity.Currency;

@Dao
public interface ChangeDao {
    @Insert
    void insertAll(Currency... currencies);

    @Query("DELETE FROM change")
    void deleteAll();

    @Delete
    void delete(Change change);

    @Insert
    void insert(Change change);

    // Получение всех Person из бд
    @Query("SELECT * FROM change")
    List<Change> getAllChange();

    @Query("SELECT COUNT(*) from change")
    int countChange();

    @Query("SELECT * FROM change WHERE date BETWEEN :from AND :to")
    List<Change> findChangeBetweenDates(Date from, Date to);
}
