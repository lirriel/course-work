package company.com.course_work.model.api.deserializer;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.Map;
import java.util.Set;

import company.com.course_work.model.entity.Currencies;
import company.com.course_work.model.entity.Currency;

public class CurrencyDeserializer implements JsonDeserializer<Currencies> {

    @Override
    public Currencies deserialize(
            JsonElement json,
            Type typeOfT,
            JsonDeserializationContext context
    ) throws JsonParseException {

        Currencies currencies = new Currencies();

        if (json.isJsonObject()) {
            JsonObject jsonObject = json.getAsJsonObject();
            Set<Map.Entry<String, JsonElement>> entries = jsonObject.get("symbols").getAsJsonObject().entrySet();

            for (Map.Entry<String, JsonElement>  entry: entries)
                currencies.addCurrency(new Currency(entry.getKey(), entry.getValue().getAsString()));

        }
        return currencies;
    }

}
