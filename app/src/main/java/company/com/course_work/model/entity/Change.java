package company.com.course_work.model.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;

import java.util.Date;

import static android.arch.persistence.room.ForeignKey.CASCADE;


@Entity(foreignKeys = {@ForeignKey(entity = Currency.class,
        parentColumns = "id",
        childColumns = "fromCurrencyID",
        onDelete = CASCADE),
        @ForeignKey(entity = Currency.class,
        parentColumns = "id",
        childColumns = "toCurrencyID",
        onDelete = CASCADE)})
public class Change {
    @PrimaryKey(autoGenerate = true)
    private long id;

    @ColumnInfo(name = "fromCurrencyID")
    private long fromCurrencyID;
    @ColumnInfo(name = "fromValue")
    private double fromValue;

    @ColumnInfo(name = "toCurrencyID")
    private long toCurrencyID;
    @ColumnInfo(name = "toValue")
    private double toValue;

    @ColumnInfo(name = "date")
    private Date date;

    public Change() {

    }

    public Change(long fromCurrency, double fromValue, long toCurrency, double toValue) {
        this.fromCurrencyID = fromCurrency;
        this.fromValue = fromValue;
        this.toCurrencyID = toCurrency;
        this.toValue = toValue;
    }

    public Change(Change change) {
        this.fromCurrencyID = change.fromCurrencyID;
        this.fromValue = change.fromValue;
        this.toCurrencyID = change.toCurrencyID;
        this.toValue = change.toValue;
        this.date = change.date;
    }

    public double getToValue() {
        return toValue;
    }

    public void setToValue(double toValue) {
        this.toValue = toValue;
    }

    public double getFromValue() {
        return fromValue;
    }

    public void setFromValue(double fromValue) {
        this.fromValue = fromValue;
    }

    public long getFromCurrencyID() {
        return fromCurrencyID;
    }

    public long getToCurrencyID() {
        return toCurrencyID;
    }

    public void setFromCurrencyID(long fromCurrencyID) {
        this.fromCurrencyID = fromCurrencyID;
    }

    public void setToCurrencyID(long toCurrencyID) {
        this.toCurrencyID = toCurrencyID;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
