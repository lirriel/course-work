package company.com.course_work.model.DAO;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import company.com.course_work.model.entity.Currency;

@Dao
public interface CurrencyDao {
    @Insert
    void insertAll(Currency... currencies);

    @Query("DELETE FROM currency")
    void deleteAll();

    @Delete
    void delete(Currency currency);

    @Insert
    void insert(Currency currency);

    // Получение всех Person из бд
    @Query("SELECT * FROM currency")
    List<Currency> getAllCurrency();

    @Query("SELECT COUNT(*) from currency")
    int countCurrency();

    @Query("SELECT * FROM currency WHERE id=:id")
    Currency getCurrencyByID(long id);

    @Query("SELECT * FROM currency WHERE symbol=:symbol")
    Currency getCurrencySymbol(String symbol);

    @Query("UPDATE Currency SET image=:image  WHERE symbol=:symbol")
    int updateCurrencyImage(String symbol, byte[] image);

    @Query("UPDATE Currency SET fave=:fave  WHERE symbol=:symbol")
    int updateCurrencyFave(String symbol, boolean fave);

    @Query("SELECT image FROM currency WHERE symbol=:symbol")
    byte[] getImage(String symbol);

    @Query("SELECT id FROM currency WHERE symbol=:symbol")
    long getId(String symbol);

    @Insert
    void insertAll(List<Currency> currencies);
}
