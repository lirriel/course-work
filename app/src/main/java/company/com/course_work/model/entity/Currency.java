package company.com.course_work.model.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import java.io.Serializable;

@Entity
public class Currency implements Serializable, Parcelable {
    @PrimaryKey(autoGenerate = true)
    private long id;

    @SerializedName("name")
    @Expose
    @ColumnInfo(name = "name")
    private String name;

    @SerializedName("symbol")
    @Expose
    @ColumnInfo(name = "symbol")
    private String symbol = "";

    @SerializedName("symbol")
    @Expose
    @ColumnInfo(typeAffinity = ColumnInfo.BLOB)
    private byte[] image;

    @SerializedName("fave")
    @Expose
    @ColumnInfo(name = "fave")
    private boolean isFavourite;

    @SerializedName("check")
    @Expose
    @Ignore
    private boolean isCheck;

    @Ignore
    public Currency(@NonNull String symbol, String name) {
        this.symbol = symbol;
        this.name = name;
    }

    public Currency() {}

    protected Currency(Parcel in) {
        id = in.readLong();
        name = in.readString();
        symbol = in.readString();
        image = in.createByteArray();
        isFavourite = in.readByte() != 0;
        isCheck = in.readByte() != 0;
    }

    public static final Creator<Currency> CREATOR = new Creator<Currency>() {
        @Override
        public Currency createFromParcel(Parcel in) {
            return new Currency(in);
        }

        @Override
        public Currency[] newArray(int size) {
            return new Currency[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public boolean isFavourite() {
        return isFavourite;
    }

    public void setFavourite(boolean favourite) {
        isFavourite = favourite;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean isCheck() {
        return isCheck;
    }

    public void setCheck(boolean check) {
        isCheck = check;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(name);
        dest.writeString(symbol);
        dest.writeByteArray(image);
        dest.writeByte((byte) (isFavourite ? 1 : 0));
        dest.writeByte((byte) (isCheck ? 1 : 0));
    }
}
