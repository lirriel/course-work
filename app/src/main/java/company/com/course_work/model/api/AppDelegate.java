package company.com.course_work.model.api;

import android.app.Application;
import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import company.com.course_work.BuildConfig;
import company.com.course_work.model.api.deserializer.ChangeDeserializer;
import company.com.course_work.model.api.deserializer.CurrencyDeserializer;
import company.com.course_work.model.api.deserializer.RatesListDeserializer;
import company.com.course_work.model.entity.Change;
import company.com.course_work.model.entity.Currencies;
import company.com.course_work.model.entity.RatesList;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import timber.log.Timber;

public class AppDelegate extends Application {

    private Api apiService;

    public static AppDelegate from(Context context) {
        return (AppDelegate) context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        initRetrofit();
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
    }

    public Api getApiService() {
        return apiService;
    }

    private void initRetrofit() {
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .build();

        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Currencies.class, new CurrencyDeserializer());
        gsonBuilder.registerTypeAdapter(Change.class, new ChangeDeserializer());
        gsonBuilder.registerTypeAdapter(RatesList.class, new RatesListDeserializer());
        Gson gson = gsonBuilder.create();

        apiService = new Retrofit.Builder()
                .baseUrl("http://data.fixer.io/api/")
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(Api.class);
    }

}

