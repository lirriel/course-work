package company.com.course_work.model.entity;

import java.util.ArrayList;
import java.util.List;

public class Currencies {
    private List<Currency> currenciesList;

    public void addCurrency(Currency currency) {
        currenciesList.add(currency);
    }

    public Currencies() {
        currenciesList = new ArrayList<>();
    }

    public List<Currency> getCurrenciesList() {
        return currenciesList;
    }

    public void setCurrenciesList(List<Currency> currenciesList) {
        this.currenciesList = currenciesList;
    }
}
