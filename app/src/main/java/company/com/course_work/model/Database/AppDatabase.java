package company.com.course_work.model.Database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.content.Context;

import company.com.course_work.model.DAO.ChangeDao;
import company.com.course_work.model.DAO.CurrencyDao;
import company.com.course_work.model.DAO.CurrencyValueDao;
import company.com.course_work.model.Database.utils.Converter;
import company.com.course_work.model.entity.Change;
import company.com.course_work.model.entity.Currency;
import company.com.course_work.model.entity.CurrencyValue;

@Database(entities = {Currency.class, Change.class, CurrencyValue.class}, version = 1)
@TypeConverters({Converter.class})
public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase INSTANCE;

    public abstract CurrencyDao getCurrencyDao();

    public abstract ChangeDao getChangeDao();

    public abstract CurrencyValueDao getCurValueDao();

    public static AppDatabase getAppDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE =
                    Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, "db_0")
                            .allowMainThreadQueries()
                            .build();
        }
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }

}