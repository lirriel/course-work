package company.com.course_work.model.api.deserializer;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.Map;
import java.util.Set;

import company.com.course_work.model.entity.RatesList;
import company.com.course_work.viewmodel.TimeManager;

public class RatesListDeserializer implements JsonDeserializer<RatesList> {

    @Override
    public RatesList deserialize(
            JsonElement json,
            Type typeOfT,
            JsonDeserializationContext context
    ) throws JsonParseException {

        RatesList ratesList = new RatesList();

        if (json.isJsonObject()) {
            JsonObject jsonObject = json.getAsJsonObject();
            String date = jsonObject.get("date").getAsString();
            Date date1 = TimeManager.dateFromString(date, "-", false);
            ratesList.setDate(date1);
            Set<Map.Entry<String, JsonElement>> entries = jsonObject.get("rates").getAsJsonObject().entrySet();

            for (Map.Entry<String, JsonElement>  entry: entries)
                ratesList.addPair(entry.getKey(), Double.parseDouble(entry.getValue().getAsString()));
        }
        return ratesList;
    }

}