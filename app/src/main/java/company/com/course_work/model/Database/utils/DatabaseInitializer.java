package company.com.course_work.model.Database.utils;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import java.util.Date;

import company.com.course_work.model.Database.AppDatabase;
import company.com.course_work.model.entity.CurrencyValue;

public class DatabaseInitializer {

    public static void populateAsync(final AppDatabase db) {
        PopulateDbAsync task = new PopulateDbAsync(db);
        task.execute();
    }

    public static void populateSync(@NonNull final AppDatabase db) {
        populateWithTestData(db);
    }

    private static void populateWithTestData(AppDatabase db) {
      //  db.getCurrencyDao().deleteAll();
        CurrencyValue currencyValue = new CurrencyValue(953, new Date(2018, 04, 1), 12);
        db.getCurValueDao().insert(currencyValue);
        currencyValue = new CurrencyValue(953, new Date(2018, 04, 3), 45);
        db.getCurValueDao().insert(currencyValue);
        currencyValue = new CurrencyValue(953, new Date(2018, 04, 4), 4);
        db.getCurValueDao().insert(currencyValue);
        currencyValue = new CurrencyValue(953, new Date(2018, 04, 5), 42);
        db.getCurValueDao().insert(currencyValue);
        currencyValue = new CurrencyValue(953, new Date(2018, 04, 10), 21);
        db.getCurValueDao().insert(currencyValue);
        currencyValue = new CurrencyValue(953, new Date(2018, 04, 12), 60);
        db.getCurValueDao().insert(currencyValue);
        currencyValue = new CurrencyValue(953, new Date(2018, 04, 17), 4);
        db.getCurValueDao().insert(currencyValue);
        currencyValue = new CurrencyValue(953, new Date(2018, 04, 20), 13);
        db.getCurValueDao().insert(currencyValue);/*
        for (int i = 0; i < 10; i++) {
            Node node = new Node(i+1, i+1);
            addNode(db, node);
            try {
                Thread.sleep(DELAY_MILLIS);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        NodeChild nodeChild = new NodeChild(1, 5);
        addConnection(db, nodeChild);
        nodeChild = new NodeChild(2, 5);
        addConnection(db, nodeChild);
        nodeChild = new NodeChild(1, 6);
        addConnection(db, nodeChild);
        */
    }

    private static class PopulateDbAsync extends AsyncTask<Void, Void, Void> {

        private final AppDatabase mDb;

        PopulateDbAsync(AppDatabase db) {
            mDb = db;
        }

        @Override
        protected Void doInBackground(final Void... params) {
            populateWithTestData(mDb);
            return null;
        }

    }
}
