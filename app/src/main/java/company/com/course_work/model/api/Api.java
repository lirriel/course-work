package company.com.course_work.model.api;

import company.com.course_work.model.entity.Change;
import company.com.course_work.model.entity.Currencies;
import company.com.course_work.model.entity.RatesList;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface Api {
    @GET("symbols")
    Call<Currencies> getCurrency(@Query("access_key") String api_key);

    @GET("latest")
    Call<Change> getConvert(
            @Query("access_key") String api_key,
            @Query("symbols") String symbols);

    @GET("latest")
    Call<RatesList> getRates(
            @Query("access_key") String api_key,
            @Query("symbols") String symbols);
}
