package company.com.course_work.model.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;

import java.util.Date;

@Entity(foreignKeys = @ForeignKey(entity = Currency.class, parentColumns = "id", childColumns = "currencyId"))
public class CurrencyValue {
    @PrimaryKey(autoGenerate = true)
    private long id;
    @ColumnInfo(name = "currencyId")
    private long currencyId;
    @ColumnInfo(name = "date")
    private Date date;
    @ColumnInfo(name = "rate")
    private double rate;

    public CurrencyValue() {}

    public CurrencyValue(long id, Date date, double rate) {
        currencyId = id;
        this.date = date;
        this.rate = rate;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public long getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(long currencyId) {
        this.currencyId = currencyId;
    }
}
